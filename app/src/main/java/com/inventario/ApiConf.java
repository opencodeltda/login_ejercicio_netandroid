package com.inventario;

import com.inventario.Intrefaces.LoadInterface;

public class ApiConf {

    private static final String BASE_URL = "http://10.0.2.2:5000/";

    public static LoadInterface getData(){
        return RetrofitAPI.getRetrofit(BASE_URL).create(LoadInterface.class);
    }
}
