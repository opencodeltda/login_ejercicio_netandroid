package com.inventario;

import com.inventario.Intrefaces.LoadInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppConfig {

    private static Retrofit retrofit =  null;
    private static LoadInterface loadInterface = null;

    private static Retrofit getClient(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl("https://127.0.0.1:5001")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public static LoadInterface loadInterface(){
        if(loadInterface == null){
            loadInterface = AppConfig.getClient().create(LoadInterface.class);
        }

        return loadInterface;
    }
}
