package com.inventario;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private EditText etUser, etPassword;
    private Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etUser = findViewById(R.id.et_user);
        etPassword = findViewById(R.id.et_passwd);
        btnLogin = findViewById(R.id.button_login);
        btnLogin.setOnClickListener(onClickLogin);
    }

    private View.OnClickListener onClickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String user = etUser.getText().toString();
            String passwd = etPassword.getText().toString();
            //
            requestData();

        }
    };



      private void requestData(){
        Call<ResponseBody> call = ApiConf.getData().getData();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                 try{
                     if(response.isSuccessful()){
                         String responseData = response.body().string();
                         Log.e("responseData", "--------->" + responseData);
                        JSONObject object = new JSONObject (response.body().string());

                     }else{
                       //ERROR...
                         Toast.makeText(MainActivity.this, "Sin Respuesta", Toast.LENGTH_SHORT).show();
                     }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Falló", Toast.LENGTH_SHORT).show();
            }
        });
    }



    /*
    private void requestData(){
        ApiConf.getData().getUsers().enqueue(new Callback<UserList>() {
                       @Override
                        public void onResponse(Call<UserList> call, Response<UserList> response) {
                           if(response.isSuccessful()) {
                               //UserList responseData = (UserList) response.body();
                               //String responseData = String.valueOf(response.body());
                              //String list = response.body();
                               Log.e("responseData", "--------->" + list.size());
                           }

                       }

                       @Override
                       public void onFailure(Call<UserList> call, Throwable t) {

                         }
                       });


    private void requestData(){
        ApiConf.getData().getUsers().enqueue(new Callback<List<UserList>>() {
            @Override
            public void onResponse(Call<List<UserList>> call, Response<List<UserList>> response) {
                if(response.isSuccessful()) {
                    //UserList responseData = (UserList) response.body();
                    //String responseData = String.valueOf(response.body());
                    List<UserList> list = response.body();
                    Log.e("responseData", "--------->" + list.size());
                }

            }

            @Override
            public void onFailure(Call<List<UserList>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Falló", Toast.LENGTH_SHORT).show();
            }
        });
    }

     */
}
